package by.qa.tasks;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import by.qa.tasks.tutby.pages.HomePage;
import by.qa.tasks.tutby.pages.SearchResultsPage;
import by.qa.tasks.tutby.pages.SearchResultsPage.SearchResultItem;

public class TutByTest {

	private Logger logger = LogManager.getLogger(TutByTest.class);
	private WebDriver driver;
	private HomePage homePage;
	private String searchQuery = "automated testing";
	private SearchResultsPage resultPage;

	@BeforeMethod
	@Parameters("browser")
	public void chooseBrowser(String browser) {

		switch (browser) {

		case "GC":

			System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\chromedriver.exe");
			driver = new ChromeDriver();
			driver.manage().window().maximize();
			break;

		case "FF":

			System.setProperty("webdriver.gecko.driver", "src\\main\\resources\\geckodriver.exe");
			driver = new FirefoxDriver();
			driver.manage().window().maximize();
			break;

		case "ED":

			System.setProperty("webdriver.edge.driver", "src\\main\\resources\\MicrosoftWebDriver.exe");
			driver = new EdgeDriver();
			driver.manage().window().maximize();
			break;

		default:

			driver.quit();
			throw new IllegalArgumentException("Passed browser code doesn't exist");

		}
	}

	@Test
	public void tutByTest() throws InterruptedException {

		logger.info("Launching browser");
		driver.get("https://tut.by");

		logger.info("Opening tut.by");
		homePage = new HomePage(driver);
		homePage.open();

		logger.info("Searching for '" + searchQuery + "' in the search field");
		resultPage = homePage.doSearch(searchQuery);

		int numberOfResults = resultPage.countResults();

		logger.info("For the search query '" + searchQuery + "' the actual number of results is '" + numberOfResults
				+ "'.");

//		String expectedResultTitle = "Minsk Automated Testing Community";
		 String expectedResultTitle = "Test automation - Wikipedia";

		boolean elementWithExpectedTitleExist = false;
		for (SearchResultItem el : resultPage.getSearchResults()) {

			if (el.getTitle().equals(expectedResultTitle)) {
				el.open();
				elementWithExpectedTitleExist = true;
				logger.info("The result with the title '" + expectedResultTitle + "' exists. Opening it's page.");
				break;
			}
		}

		if (!elementWithExpectedTitleExist) {

			String errorMessage = "There is no result with the following title: '" + expectedResultTitle + "'.";
			logger.info("Closing browser. Good bye!");
			driver.quit();
			throw new WebDriverException(errorMessage);
		}

		logger.info("Closing browser. Good bye!");
		driver.quit();
	}
}
