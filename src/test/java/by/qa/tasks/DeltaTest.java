package by.qa.tasks;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import by.qa.tasks.delta.pages.FlightResultsPage;
import by.qa.tasks.delta.pages.FlightSearchCriteria;
import by.qa.tasks.delta.pages.HomePage;
import by.qa.tasks.delta.pages.PassengerData;
import by.qa.tasks.delta.pages.PassengerInfoPage;
import by.qa.tasks.delta.pages.ReviewAndPurchasePage;
import by.qa.tasks.delta.pages.TripSummaryPage;

public class DeltaTest {

    private Logger logger = LogManager.getLogger(DeltaTest.class);
    private WebDriver driver;
    private HomePage homePage;
    private FlightSearchCriteria criteria;
    private FlightResultsPage flightResultsPage;
    private TripSummaryPage tripSummaryPage;
    private PassengerInfoPage passengerInfoPage;
    private PassengerData passengerData;
    private ReviewAndPurchasePage reviewAndPurchasePage;

    @BeforeMethod
    @Parameters("browser")
    public void chooseBrowser(String browser) {

	switch (browser) {

	case "GC":

	    System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\chromedriver.exe");
	    driver = new ChromeDriver();
	    driver.manage().window().maximize();
	    break;

	case "ED":

	    System.setProperty("webdriver.edge.driver", "src\\main\\resources\\MicrosoftWebDriver.exe");
	    driver = new EdgeDriver();
	    driver.manage().window().maximize();
	    break;

	default:

	    driver.quit();
	    throw new IllegalArgumentException("Passed browser code doesn't exist");
	}
    }

    @Test(invocationCount = 1)
    public void deltaTest() {

	logger.info("Launching browser");
	driver.get("https://delta.com");

	homePage = new HomePage(driver);
	logger.info("Opening delta.com");
	homePage.open();

	criteria = new FlightSearchCriteria();
	logger.info("Searching for: '" + criteria.toString());
	flightResultsPage = homePage.findFlights(criteria);

	logger.info("Choosing most suitable tariffs");
	tripSummaryPage = flightResultsPage.chooseFlights();

	logger.info("Entering passenger data");
	passengerInfoPage = tripSummaryPage.proceedToPassengerInfoPage();

	passengerData = new PassengerData();
	logger.info("Reviewing and completing the order");
	reviewAndPurchasePage = passengerInfoPage.proceedToReviewAndPurchasePage(passengerData);

	Assert.assertTrue(reviewAndPurchasePage.isCompletePurchaseButtonActive());

	logger.info("Closing browser. Good bye!");
	driver.quit();
    }
}
