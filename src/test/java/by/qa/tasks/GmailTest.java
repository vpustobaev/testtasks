package by.qa.tasks;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import by.qa.tasks.gmail.pages.HomePage;
import by.qa.tasks.gmail.pages.LoginPage;

public class GmailTest {

    private Logger logger = LogManager.getLogger(GmailTest.class);
    private WebDriver driver;
    private LoginPage loginPage;
    private HomePage homePage;

    @BeforeMethod
    @Parameters("browser")
    public void chooseBrowser(String browser) {

	switch (browser) {

	case "GC":

	    System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\chromedriver.exe");
	    driver = new ChromeDriver();
	    driver.manage().window().maximize();
	    break;

	default:
	    driver.quit();
	    throw new IllegalArgumentException("Passed browser code doesn't exist");
	}
    }

    @Test(invocationCount = 1)
    public void gmailTest() {

	logger.info("Launching browser");
	driver.get("https://gmail.com");

	logger.info("Opening gmail.com");
	loginPage = new LoginPage(driver);
	loginPage.open();

	logger.info("Log in into account");
	String userEmail = "vitputest@gmail.com";
	String userPassword = "15051984";
	homePage = loginPage.logIn(userEmail, userPassword);

	String actualInboxSectionTitle = driver.getTitle();
	String expectedInboxSectionTitle = "Inbox - " + userEmail + " - Gmail";
	Assert.assertEquals(actualInboxSectionTitle, expectedInboxSectionTitle);

	logger.info("Opening Sent Mail section");
	homePage.openSentMailSection();

	String actualSentMailSectionTitle = driver.getTitle();
	String expectedSentMailSectionTitle = "Sent Mail - " + userEmail + " - Gmail";
	Assert.assertEquals(actualSentMailSectionTitle, expectedSentMailSectionTitle);

	logger.info("Opening Spam section");
	homePage.openSpamSection();

	String actualSpamSectionTitle = driver.getTitle();
	String expectedSpamSectionTitle = "Spam - " + userEmail + " - Gmail";
	Assert.assertEquals(actualSpamSectionTitle, expectedSpamSectionTitle);

	logger.info("Returning to Inbox section");
	homePage.openInboxSection();

	String searchQuery = "a";
	logger.info("Searching Emails that have '" + searchQuery + "' in their subjects");
	homePage.doSearch(searchQuery);

	int actualNumberOfSearchResults = homePage.countResults();
	logger.info("Actual number of search results: '" + actualNumberOfSearchResults + "'.");
	int expectedNumberOfSearchResults = 3;
	Assert.assertEquals(actualNumberOfSearchResults, expectedNumberOfSearchResults);

	String recipientsEmail = "vpustobaev@gmail.com";
	String mailSubject = "privet";
	String mail = "Hello World";
	homePage.openNewMessageSection().sendEmail(recipientsEmail, mailSubject, mail);

	homePage.openAccountSection().signOut();

	logger.info("Closing browser. Good bye!");

	driver.quit();
    }
}
