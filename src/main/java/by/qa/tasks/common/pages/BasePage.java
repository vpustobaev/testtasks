package by.qa.tasks.common.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class BasePage {

	protected WebDriver driver;
	protected WebDriverWait wait;

	public BasePage(WebDriver driver) {

		if (driver == null) {
			throw new IllegalArgumentException("Please make sure that the passed arguments are not null.");
		}
		this.driver = driver;
		this.wait = new WebDriverWait(driver, 5);
	}

	public void initialize() {

		PageFactory.initElements(driver, this);
	}

	public void validateByTitle() {

		String url = getUrl();
		String actualTitle = driver.getTitle();
		String expectedTitle = getTitle();

		if (!actualTitle.equals(expectedTitle)) {

			String exceptionMessage = "Actual page title is '" + actualTitle + "', but the expected is '"
					+ expectedTitle + "'. Url: " + url + ". Page type: " + this.getClass().getSimpleName();
			throw new IllegalStateException(exceptionMessage);
		}
	}

	public void open() {

		String url = getUrl();
		driver.get(url);
		afterOpen();
	}

	public void afterOpen() {

		initialize();
		validateByTitle();
	}

	protected abstract String getUrl();

	protected abstract String getTitle();
}
