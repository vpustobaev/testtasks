package by.qa.tasks.tutby.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import by.qa.tasks.common.pages.BasePage;

public class HomePage extends BasePage {

	@FindBy(id = "search_from_str")
	private WebElement searchField;

	@FindBy(css = "input[name=search]")
	private WebElement searchButton;

	public HomePage(WebDriver driver) {
		super(driver);

	}

	@Override
	protected String getUrl() {

		return "https://www.tut.by/";
	}

	@Override
	protected String getTitle() {

		return "Белорусский портал TUT.BY. Новости Беларуси и мира";
	}

	public SearchResultsPage doSearch(String searchQuery) throws InterruptedException {

		searchField.sendKeys(searchQuery);
		searchButton.click();
		SearchResultsPage resultsPage = new SearchResultsPage(driver, searchQuery);
		wait.until(ExpectedConditions.titleContains(resultsPage.getTitle()));
		resultsPage.afterOpen();
		return resultsPage;
	}

}
