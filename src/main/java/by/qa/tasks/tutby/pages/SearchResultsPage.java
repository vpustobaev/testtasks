package by.qa.tasks.tutby.pages;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

import by.qa.tasks.common.pages.BasePage;

public class SearchResultsPage extends BasePage {

	@FindBy(id = "search_from_str")
	private WebElement searchField;

	@FindBy(css = "li h3")
	private List<WebElement> searchResults;

	@FindBy(css = "li h3 a+a")
	private List<WebElement> resultsHeaders;

	private String searchQuery;

	public SearchResultsPage(WebDriver driver, String searchQuery) {
		super(driver);
		this.searchQuery = searchQuery;
	}

	@Override
	protected String getUrl() {

		String result = "http://search.tut.by/?status=1&ru=1&encoding=1&page=0&how=rlv&query=";

		try {
			result += URLEncoder.encode(this.searchQuery, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			String errorMessage = "Failed to encode search query: " + this.searchQuery
					+ ". Exception with the following message was thrown: " + e.getMessage();
			Assert.fail(errorMessage);
		}

		return result;

	}

	@Override
	protected String getTitle() {

		return "TUT.BY | ПОИСК - Интернет - " + this.searchQuery;
	}

	public int countResults() {

		wait.until(ExpectedConditions.titleContains(getTitle()));
		int result = searchResults.size();
		return result;
	}

	public List<SearchResultItem> getSearchResults() {

		List<SearchResultItem> itemsList = new ArrayList<SearchResultItem>();

		for (WebElement el : resultsHeaders) {

			SearchResultItem item = new SearchResultItem(el);
			itemsList.add(item);
		}

		return itemsList;

	}

	public class SearchResultItem {

		private WebElement element;

		public SearchResultItem(WebElement element) {
			this.element = element;
		}

		public String getTitle() {
			return this.element.getText();
		}

		public String getUrl() {
			return this.element.getAttribute("href");
		}

		public void open() {

			this.element.click();
		}

	}

}
