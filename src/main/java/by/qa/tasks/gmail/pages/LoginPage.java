package by.qa.tasks.gmail.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import by.qa.tasks.common.pages.BasePage;

public class LoginPage extends BasePage {

    private WebDriverWait wait;

    @FindBy(id = "identifierId")
    private WebElement emailField;

    @FindBy(css = "div#password input")
    private WebElement passwordField;

    @FindBy(css = "span.RveJvd.snByac")
    private WebElement nextButton;

    public LoginPage(WebDriver driver) {

	super(driver);
	this.wait = super.wait;
    }

    @Override
    protected String getUrl() {

	return "https://www.gmail.com/";
    }

    @Override
    protected String getTitle() {

	return "Gmail";
    }

    public HomePage logIn(String userEmail, String userPassword) {

	wait.until(ExpectedConditions.visibilityOf(emailField));
	emailField.sendKeys(userEmail);

	wait.until(ExpectedConditions.elementToBeClickable(nextButton));
	nextButton.click();

	wait.until(ExpectedConditions.visibilityOf(passwordField));
	passwordField.sendKeys(userPassword);

	wait.until(ExpectedConditions.visibilityOf(nextButton));
	nextButton.click();

	HomePage homePage = new HomePage(driver, userEmail);
	wait.until(ExpectedConditions.titleContains(homePage.getTitle()));
	homePage.afterOpen();

	return homePage;
    }

}
