package by.qa.tasks.gmail.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import by.qa.tasks.common.pages.BasePage;

public class NewMessageSection extends BasePage {

    @FindBy(css = "textarea[name='to']")
    private WebElement recipientsInputField;

    @FindBy(css = "input[name='subjectbox']")
    private WebElement subjectInputField;

    @FindBy(css = "div.nH.Hd")
    private WebElement composeEmailDiv;

    @FindBy(css = "div[aria-label='Message Body']")
    private WebElement mailBodyField;

    @FindBy(css = "div[class='T-I J-J5-Ji aoO T-I-atl L3']")
    private WebElement sendButton;

    public NewMessageSection(WebDriver driver) {
	super(driver);
	PageFactory.initElements(driver, this);

    }

    public void sendEmail(String recipientsEmail, String mailSubject, String mail) {

	wait.until(ExpectedConditions.visibilityOf(composeEmailDiv));

	recipientsInputField.sendKeys(recipientsEmail);

	subjectInputField.sendKeys(mailSubject);

	mailBodyField.sendKeys(mail);

	sendButton.click();

	wait.until(ExpectedConditions.invisibilityOf(composeEmailDiv));

    }

    @Override
    protected String getUrl() {

	return null;
    }

    @Override
    protected String getTitle() {
	return null;
    }

}
