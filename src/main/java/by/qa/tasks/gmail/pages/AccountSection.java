package by.qa.tasks.gmail.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AccountSection {

    @FindBy(css = "a[href*='Logout']")
    private WebElement signOutButton;

    public AccountSection(WebDriver driver) {

	PageFactory.initElements(driver, this);
    }

    public void signOut() {
	signOutButton.click();
    }

}
