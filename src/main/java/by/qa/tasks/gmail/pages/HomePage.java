package by.qa.tasks.gmail.pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import by.qa.tasks.common.pages.BasePage;

public class HomePage extends BasePage {

    @FindBy(css = "a[title='Inbox']")
    private WebElement inboxLink;

    @FindBy(css = "a[title='Sent Mail']")
    private WebElement sentMailLink;

    @FindBy(css = "a[title='Starred']")
    private WebElement starredlLink;

    @FindBy(css = "span.CJ")
    private WebElement showMoreLink;

    @FindBy(css = "a[title='Spam']")
    private WebElement spamLink;

    @FindBy(css = "input#gbqfq")
    private WebElement searchField;

    @FindBy(css = "button#gbqfb")
    private WebElement searchButton;

    @FindBy(css = "div[class='BltHke nH oy8Mbf'][style=''] table[class='F cf zt'] tr")
    private List<WebElement> tableOfResults;

    @FindBy(css = "div[class='T-I J-J5-Ji T-I-KE L3']")
    private WebElement composeButton;

    @FindBy(css = "a[href*='SignOutOptions']")
    private WebElement accountSectionIcon;

    @FindBy(css = "div[aria-label='Account Information']")
    private WebElement accountInformationPopup;

    private String userEmail;

    public HomePage(WebDriver driver, String userEmail) {

	super(driver);
	this.userEmail = userEmail;
    }

    @Override
    protected String getUrl() {

	return "https://mail.google.com/mail/#inbox";
    }

    @Override
    protected String getTitle() {

	return "Inbox - " + this.userEmail + " - Gmail";
    }

    public void openSentMailSection() {

	sentMailLink.click();
	wait.until(ExpectedConditions.attributeToBe(sentMailLink, "tabindex", "0"));
    }

    public void openSpamSection() {

	Actions action = new Actions(driver);
	action.moveToElement(starredlLink).perform();

	wait.until(ExpectedConditions.visibilityOf(showMoreLink));
	showMoreLink.click();

	wait.until(ExpectedConditions.elementToBeClickable(spamLink));
	spamLink.click();
	wait.until(ExpectedConditions.titleContains("Spam - " + this.userEmail + " - Gmail"));

    }

    public void openInboxSection() {

	inboxLink.click();
	wait.until(ExpectedConditions.attributeToBe(inboxLink, "tabindex", "0"));
    }

    public void doSearch(String searchQuery) {

	searchField.click();
	searchField.sendKeys(searchQuery);
	searchButton.click();
    }

    public int countResults() {

	wait.until(ExpectedConditions.visibilityOfAllElements(tableOfResults));
	return tableOfResults.size();

    }

    public NewMessageSection openNewMessageSection() {

	composeButton.click();
	return new NewMessageSection(driver);

    }

    public AccountSection openAccountSection() {

	accountSectionIcon.click();
	wait.until(ExpectedConditions.visibilityOf(accountInformationPopup));
	return new AccountSection(driver);
    }

}
