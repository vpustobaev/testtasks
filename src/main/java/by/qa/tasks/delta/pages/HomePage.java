package by.qa.tasks.delta.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import by.qa.tasks.common.pages.BasePage;

public class HomePage extends BasePage {

    @FindBy(id = "roundTripBtn")
    private WebElement roundTripButton;

    @FindBy(id = "originCity")
    private WebElement originCityField;

    @FindBy(id = "destinationCity")
    private WebElement destinationCityField;

    @FindBy(id = "departureDate")
    private WebElement departureDateField;

    @FindBy(id = "returnDate")
    private WebElement returnDateField;

    @FindBy(css = "label#exactDaysBtn>span")
    private WebElement showExactDaysButton;

    @FindBy(css = "label#cashBtn>span")
    private WebElement showMoneyInCashButton;

    @FindBy(id = "findFlightsSubmit")
    private WebElement findFlightsButton;

    public HomePage(WebDriver driver) {

	super(driver);
    }

    public FlightResultsPage findFlights(FlightSearchCriteria criteria) {

	roundTripButton.click();

	originCityField.clear();
	originCityField.sendKeys(criteria.getOrigincity());

	destinationCityField.clear();
	destinationCityField.sendKeys(criteria.getDestinationcity());

	departureDateField.clear();
	departureDateField.sendKeys(criteria.getDeparturedate());

	returnDateField.clear();
	returnDateField.sendKeys(criteria.getReturndate());

	showExactDaysButton.click();

	showMoneyInCashButton.click();

	findFlightsButton.click();

	FlightResultsPage flightResultsPage = new FlightResultsPage(driver);
	wait.until(ExpectedConditions.titleContains(flightResultsPage.getTitle()));
	flightResultsPage.afterOpen();

	return flightResultsPage;

    }

    @Override
    protected String getUrl() {

	return "https://www.delta.com/";
    }

    @Override
    protected String getTitle() {

	return "Airline Tickets & Flights: Book Direct with Delta Air Lines - Official Site";
    }

}
