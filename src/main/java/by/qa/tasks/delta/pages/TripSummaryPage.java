package by.qa.tasks.delta.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import by.qa.tasks.common.pages.BasePage;

public class TripSummaryPage extends BasePage {

    @FindBy(id = "tripSummarySubmitBtn")
    private WebElement tripSummarySubmitButton;

    public TripSummaryPage(WebDriver driver) {

	super(driver);
    }

    public PassengerInfoPage proceedToPassengerInfoPage() {

	wait.until(ExpectedConditions.elementToBeClickable(tripSummarySubmitButton));
	tripSummarySubmitButton.click();

	PassengerInfoPage passengerInfoPage = new PassengerInfoPage(driver);
	wait.until(ExpectedConditions.titleContains(passengerInfoPage.getTitle()));
	passengerInfoPage.afterOpen();

	return passengerInfoPage;

    }

    @Override
    protected String getUrl() {

	return "https://www.delta.com/cart/activity/tripsummary.action";
    }

    @Override
    protected String getTitle() {

	return "Trip Summary : Delta Air Lines";
    }

}
