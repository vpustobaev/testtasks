package by.qa.tasks.delta.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import by.qa.tasks.common.pages.BasePage;

public class FlightResultsPage extends BasePage {

    @FindBy(css = "table[id='fareRowContainer_0'] div[id='0_0_0']")
    private WebElement flightTariff;

    private String blockingOverlay = "div[class='blockUI blockOverlay']";

    public FlightResultsPage(WebDriver driver) {
	super(driver);

    }

    public TripSummaryPage chooseFlights() {

	wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(blockingOverlay)));

	wait.until(ExpectedConditions.elementToBeClickable(flightTariff));
	flightTariff.click();

	wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(blockingOverlay)));
	
	wait.until(ExpectedConditions.elementToBeClickable(flightTariff));
	flightTariff.click();

	TripSummaryPage tripSummaryPage = new TripSummaryPage(driver);
	wait.until(ExpectedConditions.titleContains(tripSummaryPage.getTitle()));
	tripSummaryPage.afterOpen();

	return tripSummaryPage;
    }

    @Override
    protected String getUrl() {

	return "https://www.delta.com/air-shopping/findFlights.action";
    }

    @Override
    protected String getTitle() {

	return "Flight Results : Find & Book Airline Tickets : Delta Air Lines";
    }

}
