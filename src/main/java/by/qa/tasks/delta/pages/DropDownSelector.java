package by.qa.tasks.delta.pages;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class DropDownSelector {

    private WebDriver driver;
    private WebElement buttonSelector;
    private By listSelector;

    public DropDownSelector(WebDriver driver, By buttonSelector, By listSelector) {
	this.driver = driver;
	this.buttonSelector = driver.findElement(buttonSelector);
	this.listSelector = listSelector;
    }

    public void select(String optionToChoose) {

	this.buttonSelector.click();

	List<WebElement> list = this.driver.findElements(listSelector);

	for (WebElement el : list) {

	    if (el.getText().equals(optionToChoose)) {

		JavascriptExecutor je = (JavascriptExecutor) driver;
		je.executeScript("arguments[0].scrollIntoView(true);", el);
		new Actions(driver).moveToElement(el).perform();
		el.click();
		break;
	    }
	}
    }
}
