package by.qa.tasks.delta.pages;

public class FlightSearchCriteria {

    private String originCity = "JFK";
    private String destinationCity = "SVO";
    private String departureDate = "08232017";
    private String returnDate = "08302017";

    public String getOrigincity() {
	return originCity;
    }

    public String getDestinationcity() {
	return destinationCity;
    }

    public String getDeparturedate() {
	return departureDate;
    }

    public String getReturndate() {
	return returnDate;
    }

    @Override
    public String toString() {
	return "FlightSearchCriteria [originCity=" + originCity + ", destinationCity=" + destinationCity
		+ ", departureDate=" + departureDate + ", returnDate=" + returnDate + "]";
    }

}
