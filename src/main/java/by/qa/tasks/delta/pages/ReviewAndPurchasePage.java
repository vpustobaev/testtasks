package by.qa.tasks.delta.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import by.qa.tasks.common.pages.BasePage;

public class ReviewAndPurchasePage extends BasePage {

    @FindBy(id = "continue_button")
    private WebElement completePurchaseButton;

    public ReviewAndPurchasePage(WebDriver driver) {
	super(driver);
    }

    public boolean isCompletePurchaseButtonActive() {

	wait.until(ExpectedConditions.elementToBeClickable(completePurchaseButton));
	return completePurchaseButton.isEnabled();
    }

    @Override
    protected String getUrl() {

	return "https://www.delta.com/cart/activity/reviewFlight.action";
    }

    @Override
    protected String getTitle() {

	return "Review and Purchase : Delta Air Lines";
    }

}
