package by.qa.tasks.delta.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import by.qa.tasks.common.pages.BasePage;

public class PassengerInfoPage extends BasePage {

    @FindBy(id = "firstName0")
    private WebElement firstNameField;

    @FindBy(id = "lastName0")
    private WebElement lastNameField;

    @FindBy(id = "telephoneNumber0")
    private WebElement phoneNumberField;

    @FindBy(id = "emgcFirstName_0")
    private WebElement emergencyPersonsFirstNameField;

    @FindBy(id = "emgcLastName_0")
    private WebElement emergencyPersonsLastNameField;

    @FindBy(id = "emgcPhoneNumber_0")
    private WebElement emergencyPersonsPhoneNumberField;

    @FindBy(id = "email")
    private WebElement emailField;

    @FindBy(id = "reEmail")
    private WebElement repeatEmailField;

    @FindBy(id = "paxReviewPurchaseBtn")
    private WebElement continueButton;

    private DropDownSelector genderSelector;

    private DropDownSelector monthSelector;

    private DropDownSelector daySelector;

    private DropDownSelector yearSelector;

    private DropDownSelector emergencyPersonsCountryCodeSelector;

    private DropDownSelector deviceTypeSelector;

    private DropDownSelector countryCodeSelector;

    public PassengerInfoPage(WebDriver driver) {

	super(driver);
	wait.until(ExpectedConditions.elementToBeClickable( By.id("gender0-button")));
	this.genderSelector = new DropDownSelector(driver, By.id("gender0-button"), By.cssSelector("#gender0-menu li"));
	this.monthSelector = new DropDownSelector(driver, By.id("month0-button"), By.cssSelector("#month0-menu li"));
	this.daySelector = new DropDownSelector(driver, By.id("day0-button"), By.cssSelector("#day0-menu li"));
	this.yearSelector = new DropDownSelector(driver, By.id("year0-button"), By.cssSelector("#year0-menu li"));
	this.emergencyPersonsCountryCodeSelector = new DropDownSelector(driver, By.id("emgcCountry_0-button"),
		By.cssSelector("#emgcCountry_0-menu li"));
	this.deviceTypeSelector = new DropDownSelector(driver, By.id("deviceType-button"),
		By.cssSelector("#deviceType-menu li"));
	this.countryCodeSelector = new DropDownSelector(driver, By.id("countryCode0-button"),
		By.cssSelector("#countryCode0-menu li"));
    }

    public ReviewAndPurchasePage proceedToReviewAndPurchasePage(PassengerData passengerData) {

	wait.until(ExpectedConditions.elementToBeClickable(firstNameField));
	firstNameField.sendKeys(passengerData.getFirstName());

	lastNameField.sendKeys(passengerData.getLastName());

	genderSelector.select(passengerData.getGender());

	monthSelector.select(passengerData.getBirthMonth());

	daySelector.select(passengerData.getBirthDay());

	yearSelector.select(passengerData.getBirthYear());

	emergencyPersonsFirstNameField.sendKeys(passengerData.getFirstName());

	emergencyPersonsLastNameField.sendKeys(passengerData.getLastName());

	emergencyPersonsCountryCodeSelector.select(passengerData.getCountryCode());

	emergencyPersonsPhoneNumberField.sendKeys(passengerData.getTelephoneNumber());

	deviceTypeSelector.select(passengerData.getDeviceType());

	countryCodeSelector.select(passengerData.getCountryCode());

	phoneNumberField.sendKeys(passengerData.getTelephoneNumber());

	emailField.sendKeys(passengerData.getEmail());

	repeatEmailField.sendKeys(passengerData.getEmail());

	continueButton.click();

	ReviewAndPurchasePage reviewAndPurchasePage = new ReviewAndPurchasePage(driver);
	wait.until(ExpectedConditions.titleContains(reviewAndPurchasePage.getTitle()));
	reviewAndPurchasePage.afterOpen();

	return reviewAndPurchasePage;

    }

    @Override
    protected String getUrl() {

	return "https://www.delta.com/cart/activity/passengerinfo.action";
    }

    @Override
    protected String getTitle() {

	return "Passenger Information Page : Delta Air Lines";
    }

}
