package by.qa.tasks.delta.pages;

public class PassengerData {

    private String firstName = "Vitaly";

    private String lastName = "Pustobaev";

    private String gender = "Male";

    private String birthMonth = "May";

    private String birthDay = "15";

    private String birthYear = "1984";

    private String deviceType = "Cell";

    private String countryCode = "Belarus (375)";

    private String telephoneNumber = "256224477";

    private String email = "vpustobaev@gmail.com";

    public String getFirstName() {
	return firstName;
    }

    public String getLastName() {
	return lastName;
    }

    public String getGender() {
	return gender;
    }

    public String getBirthMonth() {
	return birthMonth;
    }

    public String getBirthDay() {
	return birthDay;
    }

    public String getBirthYear() {
	return birthYear;
    }

    public String getDeviceType() {
	return deviceType;
    }

    public String getCountryCode() {
	return countryCode;
    }

    public String getTelephoneNumber() {
	return telephoneNumber;
    }

    public String getEmail() {
	return email;
    }

}
